class AddCategoryToProducts < ActiveRecord::Migration[7.0]
  def change
    add_column :products, :category, :string
    add_reference :products, :category, null: false, foreign_key: true
  end
end
