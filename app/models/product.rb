class Product < ApplicationRecord

  include PgSearch::Model
  pg_search_scope :search_full_text, against: {
    title: 'A',
    description: 'B',
  }

  ORDER_BY = {
    new: "created_at DESC",
    old: "created_at ASC",
    expensive: "price DESC",
    cheap: "price ASC",
  }

  has_one_attached :photo
  validates :title, :description, :price, presence: true
  belongs_to :category
end
